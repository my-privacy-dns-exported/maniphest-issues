# Maniphest

Maniphest is the name of our old issue board within Phabricator. However Phabricator has announced the EOL and we have therefor converted to use GitLab as our workboard.

This particular repo hold any issues that was ever poster inside Phabricator, and they will continue to work as anchors for incoming links to redirect them to new locations.

A note to this is private/hidden issues will be moved and then made silence by marking them `Confidentiality`

This means nobody ever should be posting any new issues ever!!

Best recards @mypdns

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/X8X37FUGU)
